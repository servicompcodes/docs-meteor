Desplegar Meteor en Heroku
==========================

- Crear BD en MLab
- Crear nuevo usuario para la BD recién creada
- Obtener la URI completa de la BD, que tiene la forma:
```
mongodb://<dbuser>:<dbpassword>@ds017231.mlab.com:17231/db-name
```
*Reemplazar `<dbuser>` y `<dbpassword>` con los creados en el paso anterior.*

## Paso 1.
Ir al directorio de la aplicación, abrir una terminal y escribir:
```
heroku create <nombre-app>
```
*Reemplazar `<nombre-app>` con el nombre de tu aplicación*

## Paso 2.
Configuramos la URL de Heroku
```
heroku config:set ROOT_URL=https://<nombre-app>.herokuapp.com
```
*Reemplazar `<nombre-app>` con el nombre de tu aplicación*

## Paso 3.
Configuramos la URI de MLab
```
heroku config:set MONGO_URL=<mlab-uri>
```
*Reemplazar `<mlab-uri>` con la URL descrita arriba*

## Paso 4.
Configurar **Buildpack** (el buildpack es una serie de archivos que le dice a Heroku como armar nuestra aplicación). [Buildpack Horse](https://github.com/AdmitHub/meteor-buildpack-horse) es mantenida por la comunidad de meteor y sirve para desplegar nuestra aplicación en Heroku. Escribir:
```
heroku buildpacks:set https://github.com/AdmitHub/meteor-buildpack-horse
```

## Paso 5.
Finalmente, hacer un **PUSH** a nuestra rama MASTER del repositorio GIT en Heroku mediante:
```
git push heroku master
```

## Paso Extra.
Si la aplicacion de Meteor no se encuentra en el directorio raíz de nuestro reposotorio especificar la ruta con el siguiente comando:
```
heroku config:set METEOR_APP_DIR='<your-app-dir>/'
```
*Reemplazar `<your-app-dir>` con la el nombre de la carpeta donde se encuentra nuestra instalación de Meteor*

***Este tutorial está basado principalmente en el que se encuentra en [este link](https://medium.com/nona-web/deploying-and-hosting-meteor-on-heroku-mongolab-for-free-37050a3ebd7e).***
