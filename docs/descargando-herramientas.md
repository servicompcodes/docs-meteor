
Descargando Herramientas
================

*Fecha:"2017-04-30 02:20"*

### Desarrollo
  - [GitKraken](https://www.gitkraken.com/) (GUI para Git)
  - [Atom](https://atom.io/) (Editor de texto plano)

### Despliegue
  - [Heroku Toolbelt](https://devcenter.heroku.com/articles/heroku-cli) (Línea de comandos de Heroku)
