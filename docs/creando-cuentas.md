
Creando Cuentas
================

*Fecha:"2017-04-30 02:31"*

Existe una serie de cuentas que debemos crear en distintos servicios para que nuestro desarrollo sea exitoso.

De todas ellas, **debemos almacenar con mucho cuidado** las credenciales, pues son escenciales para el proyecto.

### Desarrollo

- [BitBucket](https://bitbucket.org/account/signup/) y/o [GitHub](https://github.com/join) (Repositorios GIT que sirven para guardar distintas versiones de tu app).

### Despliegue

- [Heroku](https://signup.heroku.com/) (Hosting y subdominio gratuito, despliegue automático con comandos git, estadísticas, etc.)

- [MLab](https://mlab.com/signup/) (Hosting de bases de datos MongoDB)
