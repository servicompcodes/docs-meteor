
Crear Aplicacion
================

*Fecha: "2017-04-29 23:42"*

### Pasos

- Crear cuenta en GitHub o Bitbucket
- Enlazar la cuenta a GitKraken
- Crear Repositorio Local y Remoto con GitKraken
- Instalar Meteor

### Enlazar cuenta a GitKraken

Ir a *File->Preferences...*
![Enlazar Cuenta](images/2017-04-30-00.27.52.png)

Hacer click en *Authentication*
![](images/2017-04-30-00.28.09.png)

Elegir el tipo de cuenta *GitHub o BitBucket* y Hacer click en *Connect to GitHub o BitBucket*
![Inicializar Repositorio(1)](images/2017-04-30-00.28.17.png)

Se nos abrirá una pestaña en el navegador donde debemos loguearnos con las credenciales de nuestra cuenta.
![Inicializar Repositorio(1)](images/2017-04-30-00.29.12.png)

Completado este paso tendremos enlazada cuentra cuenta a la aplicación.

![Inicializar Repositorio(1)](images/2017-04-30-00.29.26.png)

![Inicializar Repositorio(1)](images/2017-04-30-00.31.19.png)


### Crear Repositorio Local y Remoto con GitKraken

Para crear tu proyecto, primero debes inicializar un repositorio local y uno remoto para tener respaldo de nuestro código (siempre es bueno tener nuestro trabajo respaldado).

Esto lo haremos con GitKraken, una GUI diseñada para manejar de forma visual los repositorios git.

Para crear nuestro repositorio iremos a *File->Init Repo*

![Inicializar Repositorio(1)](images/2017-04-29-23.53.09.png)

Luego, debemos rellenar los campos que aparecen:

![Inicializar Repositorio(2)](images/2017-04-29-23.53.36.png)

### Instalar meteor


Para crear una aplicacion en meteor simplemente debes escribir el siguente comando en la ruta de tu repositorio. Esto creara una nueva carpeta dentro de él con el nombre de tu aplicación.
```
meteor create <nombreAplicacion>
```
