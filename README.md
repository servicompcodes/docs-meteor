Docs-Meteor
===========

Docs-Meteor es una recopilación de información relacionada a cómo trabajar de forma eficiente los proyectos desarrollados sobre  [Meteor](https://www.meteor.com/).

Pretende ser una guía en español que determine lineamientos de trabajo en el entorno de desarrollo, que hagan posible llevar en poco tiempo una WebApp reactiva desde desarrollo **alpha** a **producción**.

La documentación oficial de este asombroso framework javascript, se encuentra en [este link](http://docs.meteor.com/#/full/).

### Tabla de Contenidos

- Preparando el ambiente de desarrollo
    - [Descargando Herramientas](docs/descargando-herramientas.md)
    - [Creando Cuentas](docs/creando-cuentas.md)
        - Configurando y enlazando las cuentas
- Desarrollando Aplicación
    - [Crear Aplicación](docs/crear-aplicacion.md)
- Desplegar Aplicación
    - [Heroku](docs/desplegar-heroku.md)
